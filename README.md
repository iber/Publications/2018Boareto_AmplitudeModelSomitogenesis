### Source code of the analysis of the following manuscript:

Positional information encoded in the dynamic differences between neighbouring oscillators during vertebrate segmentation. 
Marcelo Boareto, Tomas Tomka, and Dagmar Iber.
https://www.biorxiv.org/content/early/2018/03/25/286328
